package com.jakala.rep;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jakala.model.Dipendente;
import com.jakala.model.Supermercato;

@Repository
public interface DipendenteRepository extends JpaRepository<Dipendente, Long>{
	
	
	
	List<String> findBySupermercato(Supermercato supermercato);
	
	List<Dipendente> findByOrtofruttaId(Long id);
	
	List<Dipendente> findBySalumeriaId(Long id);
	
	List<Dipendente> findByMagazzinoId(Long id);
	
	List<Dipendente> findByCassaId(Long id);

	Long countNomeByOrtofruttaId(Long id);
	
	Long countNomeBySalumeriaId(Long id);
	
	Long countNomeByMagazzinoId(Long id);
	
	Long countNomeByCassaId(Long id);
	
	Dipendente findByNome(String name);
	
}
