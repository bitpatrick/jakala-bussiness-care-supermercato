package com.jakala.rep;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jakala.model.Supermercato;

@Repository
public interface SupermercatoRepository extends JpaRepository<Supermercato, Long> {
	
	Supermercato findByNome(String nome);

}
