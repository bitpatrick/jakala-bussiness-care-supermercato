package com.jakala.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.jakala.exception.DipendenteNoJobException;
import com.jakala.exception.EccessoDipendentiNelReparto;

import lombok.Getter;
import lombok.Setter;

@Table(name = "reparti_magazzino")
@Entity(name = "repartiMagazzino")
@Getter
@Setter
public class Magazzino implements Reparto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Transient
	private static final int numeroMaxDipendentiReparto = 4;
	
	private int numeroDipendentiReparto = 0;
		
	@OneToOne
	private Supermercato supermercato;
	
	@OneToMany(mappedBy = "magazzino", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Dipendente> dipendenti = new HashSet<Dipendente>(4);
	
	public void addDipendente(Dipendente dipendente) {
		
		if ( this.dipendenti.size() > numeroMaxDipendentiReparto-1 ) {
			throw new EccessoDipendentiNelReparto("Eccesso dipendenti nel reparto: non può essere maggiore di 4");
		} else {
		this.dipendenti.add(dipendente);
		dipendente.setMagazzino(this);
		numeroDipendentiReparto++;
		}
	}
	
	public void removeDipendente(Dipendente dipendente) {
		
		if ( dipendente.getOrtofrutta() == null && dipendente.getSalumeria() == null && dipendente.getCassa() == null ) {
			
			throw new DipendenteNoJobException("il dipendente deve essere associato ad almeno un reparto");
		} else {
			this.dipendenti.remove(dipendente);
			dipendente.setMagazzino(null);
			numeroDipendentiReparto--;
		}

	}
	
}
