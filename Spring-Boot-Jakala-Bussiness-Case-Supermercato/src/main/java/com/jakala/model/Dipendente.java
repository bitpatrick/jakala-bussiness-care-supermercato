package com.jakala.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "dipendenti")
@Entity(name = "dipendenti")
@Getter
@Setter
@NoArgsConstructor// viene utilizzato da JPA quando recupero una entità dal db
public class Dipendente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nome;
	
	@ManyToOne
	private Supermercato supermercato;
	
	@ManyToOne
	private Ortofrutta ortofrutta;
	
	@ManyToOne
	private Salumeria salumeria;
	
	@ManyToOne
	private Magazzino magazzino;
	
	@ManyToOne
	private Cassa cassa;

	public Dipendente(String nome) {
		super();
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dipendente )) return false;
        return id != null && id.equals(((Dipendente) o).getId());
    }
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
	
	
	

}
