package com.jakala.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.jakala.exception.RepartoIsAlreadyExist;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Table(name = "supermercati")
@Entity(name = "supermercati")
@Getter
@Setter
@NoArgsConstructor // viene utilizzato da JPA quando recupero un entità dal db 
public class Supermercato {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nome;
	
	@OneToMany(mappedBy = "supermercato", cascade = CascadeType.ALL)
	private Set<Dipendente> dipendenti = new HashSet<Dipendente>();
	
	public void addDipendente(Dipendente dipendente) {
		
		this.dipendenti.add(dipendente);
		dipendente.setSupermercato(this);
	}
	
	
	//REPARTI SUPERMERCATO
	@OneToOne(mappedBy = "supermercato", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Ortofrutta ortofrutta;
	
	@OneToOne(mappedBy = "supermercato", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Salumeria salumeria;
	
	@OneToOne(mappedBy = "supermercato", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Magazzino magazzino;
	
	@OneToOne(mappedBy = "supermercato", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Cassa cassa;
	
	
	
	public void addReparto(Reparto reparto) {
		
		if ( reparto instanceof Ortofrutta && this.ortofrutta == null ) {
			
			Ortofrutta ortofrutta = (Ortofrutta) reparto;
			this.ortofrutta = ortofrutta;
			ortofrutta.setSupermercato(this);
			
		} else if ( reparto instanceof Salumeria && this.salumeria == null ) {
			
			Salumeria salumeria = (Salumeria) reparto;
			this.salumeria = salumeria;
			salumeria.setSupermercato(this);
		
		} else if ( reparto instanceof Magazzino && this.magazzino == null) {
			
			Magazzino magazzino = (Magazzino) reparto;
			this.magazzino = magazzino;
			magazzino.setSupermercato(this);
			
		} else if ( reparto instanceof Cassa && this.cassa == null ) {
			
			Cassa cassa = (Cassa) reparto;
			this.cassa = cassa;
			cassa.setSupermercato(this);
		
		} else {
			throw new RepartoIsAlreadyExist("Reparto già esistente");
		}
	
	}

	
	//COSTRUTTORI
	public Supermercato(String nome) {
		super();
		this.nome = nome;
	}
	
	
	
	
	

	
	
}
