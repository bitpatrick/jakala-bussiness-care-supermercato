package com.jakala.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.jakala.exception.DipendenteNoJobException;
import com.jakala.exception.EccessoDipendentiNelReparto;

import lombok.Getter;
import lombok.Setter;

@Table(name = "reparti_salumeria")
@Entity(name = "repartiSalumeria")
@Getter
@Setter
public class Salumeria implements Reparto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	private Supermercato supermercato;
	
	@OneToMany(mappedBy = "salumeria", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Dipendente> dipendenti = new HashSet<Dipendente>(4);
	
	@Transient
	private static final int numeroMaxDipendentiReparto = 4;
	
	private int numeroDipendentiReparto = 0;
	
	public void addDipendente(Dipendente dipendente) {
		
		if ( this.dipendenti.size() > numeroMaxDipendentiReparto-1 ) {
			throw new EccessoDipendentiNelReparto("Eccesso dipendenti nel reparto: non può essere maggiore di 4");
		} else {
		this.dipendenti.add(dipendente);
		dipendente.setSalumeria(this);
		numeroDipendentiReparto++;
		}
		
	}
	
	public void removeDipendente(Dipendente dipendente) {
		
		if ( dipendente.getOrtofrutta() == null && dipendente.getMagazzino() == null && dipendente.getCassa() == null ) {
			
			throw new DipendenteNoJobException("il dipendente deve essere associato ad almeno un reparto");
		} else {
			this.dipendenti.remove(dipendente);
			dipendente.setSalumeria(null);
			numeroDipendentiReparto--;
		}

	}
	

}
