package com.jakala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJakalaBussinessCaseSupermercatoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJakalaBussinessCaseSupermercatoApplication.class, args);
	}

}
