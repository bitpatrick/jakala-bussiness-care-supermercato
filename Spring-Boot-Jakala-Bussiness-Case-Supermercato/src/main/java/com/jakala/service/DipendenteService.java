package com.jakala.service;

import com.jakala.model.Dipendente;
import com.jakala.model.Reparto;

public interface DipendenteService {
	
	void cambioReparto(Dipendente dipendente, Reparto vecchioReparto, Reparto nuovoReparto);
	
	void assunzione(Dipendente dipendente, Reparto reparto);

}
