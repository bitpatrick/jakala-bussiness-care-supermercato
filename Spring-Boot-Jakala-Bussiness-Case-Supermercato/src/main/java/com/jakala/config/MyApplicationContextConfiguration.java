package com.jakala.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jakala.model.Cassa;
import com.jakala.model.Magazzino;
import com.jakala.model.Ortofrutta;
import com.jakala.model.Salumeria;
import com.jakala.model.Supermercato;

@Configuration
public class MyApplicationContextConfiguration {
	
	@Bean
	public Supermercato supermercatoPewex() {
		
		//costruzione oggetto supermercato
		Supermercato supermercatoPewex = new Supermercato("Pewex");
		
		//aggiunta reparti al supermercato
		supermercatoPewex.addReparto(new Ortofrutta());
		supermercatoPewex.addReparto(new Salumeria());
		supermercatoPewex.addReparto(new Magazzino());
		supermercatoPewex.addReparto(new Cassa());
				
		//restituzione oggetto
		return supermercatoPewex;

	}
	
	


}
