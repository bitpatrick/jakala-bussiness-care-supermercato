package com.jakala.seed;

import java.util.Set;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.jakala.config.MyApplicationContextConfiguration;
import com.jakala.model.Dipendente;
import com.jakala.model.Ortofrutta;
import com.jakala.model.Salumeria;
import com.jakala.model.Supermercato;
import com.jakala.rep.DipendenteRepository;
import com.jakala.rep.SupermercatoRepository;
import com.jakala.service.DipendenteService;
import com.jakala.serviceImpl.DipendenteServiceImpl;

@Component
public class Run implements CommandLineRunner {
	

	private Logger LOG = LoggerFactory.getLogger(Run.class);
	
	ApplicationContext ctx = new AnnotationConfigApplicationContext(MyApplicationContextConfiguration.class);
	
	@Autowired
	DipendenteRepository dipendenteRepository;
	
	@Autowired
	SupermercatoRepository supermercatoRepository;
	
	@Autowired
	DipendenteServiceImpl dipendenteServiceImpl;
	
	@Override
	public void run(String... args) throws Exception {
		
	
	LOG.info("run method of CommandLineRunner is running...");
	
	Supermercato supermercatoPewex = (Supermercato) ctx.getBean("supermercatoPewex");
	
	
	//CREAZIONE DIPENDENTI
	Dipendente dipendente1 = new Dipendente("Marco");
	Dipendente dipendente2 = new Dipendente("Teresa");
	Dipendente dipendente3 = new Dipendente("Paolo");
	Dipendente dipendente4 = new Dipendente("Lucia");
	Dipendente dipendente5 = new Dipendente("Giorgio");
	Dipendente dipendente6 = new Dipendente("Nicola");
	Dipendente dipendente7 = new Dipendente("Silvia");
	
	
	//AGGIUNTA DIPENDENTI AL SUPERMERCATO
	supermercatoPewex.addDipendente(dipendente1);
	supermercatoPewex.addDipendente(dipendente2);
	supermercatoPewex.addDipendente(dipendente3);
	supermercatoPewex.addDipendente(dipendente4);
	supermercatoPewex.addDipendente(dipendente5);
	supermercatoPewex.addDipendente(dipendente6);
	supermercatoPewex.addDipendente(dipendente7);
	
	//ASSOCIAZIONE DIPENDENTI AI REPARTI DEL SUPERMERCATO PEWEX
	
	//Reparto Ortofrutta
	supermercatoPewex.getOrtofrutta().addDipendente(dipendente1);
	supermercatoPewex.getOrtofrutta().addDipendente(dipendente3);
	
	//Reparto Salumeria
	supermercatoPewex.getSalumeria().addDipendente(dipendente3);
	supermercatoPewex.getSalumeria().addDipendente(dipendente5);
	supermercatoPewex.getSalumeria().addDipendente(dipendente7);
	
	//Reparto Magazzino
	supermercatoPewex.getMagazzino().addDipendente(dipendente4);
	supermercatoPewex.getMagazzino().addDipendente(dipendente5);
	
	//reparto Cassa
	supermercatoPewex.getCassa().addDipendente(dipendente2);
	supermercatoPewex.getCassa().addDipendente(dipendente4);
	
	//Marco passa al magazzino
	dipendenteServiceImpl.cambioReparto(dipendente1, supermercatoPewex.getOrtofrutta(), supermercatoPewex.getMagazzino());
	
	dipendenteServiceImpl.assunzione(new Dipendente("Stefano"), supermercatoPewex.getCassa());
	
	//salvataggio supermercato nel db
	supermercatoRepository.save(supermercatoPewex);
	
	//Supermercato supermercatoRecuperatoDalDB = supermercatoRepository.findById(supermercatoPewex.getId()).get();
	
	//dipendenteRepository.findBySalumeria(supermercatoPewex.getSalumeria()).stream().map(s -> s.getNome()).forEach(System.out::println);
	
	
	
	
	LOG.info("run method of CommandLineRunner has been complete...");
	}

}
