package com.jakala.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class AssunzioneExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(DipendenteException.class)
	protected ResponseEntity<Object> handleAssunzioneException(DipendenteException ex) {
		
		ApiError apiError = new ApiError(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		
		return builResponseEntity(apiError);
		
		
	}
	
	private ResponseEntity<Object> builResponseEntity(ApiError apiError) {
		return new ResponseEntity<Object>(apiError,apiError.getStatus());
	}

}
