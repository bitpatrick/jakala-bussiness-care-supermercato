package com.jakala.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jakala.model.Cassa;
import com.jakala.model.Dipendente;
import com.jakala.model.Magazzino;
import com.jakala.model.Ortofrutta;
import com.jakala.model.Reparto;
import com.jakala.model.Salumeria;
import com.jakala.service.DipendenteService;

@Service
public class DipendenteServiceImpl implements DipendenteService {
	
	Logger LOG = LoggerFactory.getLogger(DipendenteServiceImpl.class);

	@Override
	public void cambioReparto(Dipendente dipendente, Reparto vecchioReparto, Reparto nuovoReparto) {

		if ( nuovoReparto instanceof Ortofrutta) {
			
			Ortofrutta ortofrutta = (Ortofrutta) nuovoReparto;
			ortofrutta.addDipendente(dipendente);
			
		} else if ( nuovoReparto instanceof Salumeria ) {
			
			Salumeria salumeria = (Salumeria) nuovoReparto;
			salumeria.addDipendente(dipendente);
			
		} else if ( nuovoReparto instanceof Magazzino ) {
			
			Magazzino magazzino = (Magazzino) nuovoReparto;
			magazzino.addDipendente(dipendente);
			
		} else if ( nuovoReparto instanceof Cassa ) {
			
			Cassa cassa = (Cassa) nuovoReparto;
			cassa.addDipendente(dipendente);
			
		}
		
		if ( vecchioReparto instanceof Ortofrutta && dipendente.getOrtofrutta() != null) {
			
			Ortofrutta ortofrutta = (Ortofrutta) vecchioReparto;
			ortofrutta.removeDipendente(dipendente);
		
		} else if ( vecchioReparto instanceof Salumeria && dipendente.getSalumeria() != null ) {
			
			Salumeria salumeria = (Salumeria) vecchioReparto;
			salumeria.removeDipendente(dipendente);
			
		} else if ( vecchioReparto instanceof Magazzino && dipendente.getMagazzino() != null ) {
			
			Magazzino magazzino = (Magazzino) vecchioReparto;
			magazzino.removeDipendente(dipendente);
			
		} else if ( vecchioReparto instanceof Cassa && dipendente.getCassa() != null) {
			
			Cassa cassa = (Cassa) vecchioReparto;
			cassa.removeDipendente(dipendente);
		} else {
			LOG.info("il vecchio reparto non è associato al dipendente");
		}

	}

	@Override
	public void assunzione(Dipendente dipendente, Reparto reparto) {
		
		if ( reparto instanceof Ortofrutta) {
			
			Ortofrutta ortofrutta = (Ortofrutta) reparto;
			ortofrutta.getSupermercato().addDipendente(dipendente);
			ortofrutta.addDipendente(dipendente);
			
		} else if ( reparto instanceof Salumeria ) {
			
			Salumeria salumeria = (Salumeria) reparto;
			salumeria.getSupermercato().addDipendente(dipendente);
			salumeria.addDipendente(dipendente);
			
		} else if ( reparto instanceof Magazzino ) {
			
			Magazzino magazzino = (Magazzino) reparto;
			magazzino.getSupermercato().addDipendente(dipendente);
			magazzino.addDipendente(dipendente);
			
		} else if ( reparto instanceof Cassa ) {
			
			Cassa cassa = (Cassa) reparto;
			cassa.getSupermercato().addDipendente(dipendente);
			cassa.addDipendente(dipendente);
			
		}

		
		
	}
	
	
	
	

}
