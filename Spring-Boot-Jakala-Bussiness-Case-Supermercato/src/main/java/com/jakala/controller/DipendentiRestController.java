package com.jakala.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.jakala.datamodel.DipendentePewex;

import com.jakala.exception.DipendenteException;
import com.jakala.model.Dipendente;
import com.jakala.model.Reparto;
import com.jakala.model.Supermercato;
import com.jakala.rep.DipendenteRepository;

import com.jakala.rep.SupermercatoRepository;
import com.jakala.service.DipendenteService;

@RestController
@RequestMapping("/api")
public class DipendentiRestController {
	
	@Autowired
	DipendenteService dipendenteService;
	
	@Autowired
	DipendenteRepository dipendenteRepository;
	
	@Autowired
	SupermercatoRepository supermercatoRepository;
	
	@GetMapping("/testGet")
	public @ResponseBody String allAccess() {
		return "Calling GET /testGet";
	}

	@GetMapping("/dipendentiPewex")
	public ResponseEntity<List<String>> getDipendentiPewex() {
		List<String> dipendentiPewex = dipendenteRepository.findBySupermercato(supermercatoPewex());
		return new ResponseEntity<List<String>>(dipendentiPewex, HttpStatus.OK);
	}
	
	@GetMapping("/dipendentiOrtofruttaPewex")
	public ResponseEntity<List<List<String>>> getDipendentiOrtofruttaPewex() {
		
		List<Dipendente> dipendentiOrtofruttaPewex = dipendenteRepository.findByOrtofruttaId(supermercatoPewex().getOrtofrutta().getId());
		List<String> nomeDipendenti = dipendentiOrtofruttaPewex.stream().map(d -> d.getNome()).collect(Collectors.toList());
		
		Long numeroDipendenti = dipendenteRepository.countNomeByOrtofruttaId(supermercatoPewex().getOrtofrutta().getId());
		String numeroPostiDisponibili = "numero posti disponibili: " + ( 4 - numeroDipendenti ) ;
		String numeroPostiOccupati = "numero posti occupati: " +  numeroDipendenti;
		
		List<String> numeroPosti = new ArrayList<String>( Arrays.asList(
				numeroPostiDisponibili,
				numeroPostiOccupati
				));
		
		List<List<String>> array = new ArrayList<List<String>>( Arrays.asList(
				numeroPosti,
				nomeDipendenti
				));
		
		
		return new ResponseEntity<List<List<String>>>(array, HttpStatus.OK);
	}
	
	@GetMapping("/dipendentiSalumeriaPewex")
	public ResponseEntity<List<List<String>>> getDipendentiSalumeriaPewex() {
		List<Dipendente> dipendentiSalumeriaPewex = dipendenteRepository.findBySalumeriaId(supermercatoPewex().getSalumeria().getId());
		List<String> nomeDipendenti = dipendentiSalumeriaPewex.stream().map(d -> d.getNome()).collect(Collectors.toList());
		
		Long numeroDipendenti = dipendenteRepository.countNomeBySalumeriaId(supermercatoPewex().getOrtofrutta().getId());
		String numeroPostiDisponibili = "numero posti disponibili: " + ( 4 - numeroDipendenti ) ;
		String numeroPostiOccupati = "numero posti occupati: " +  numeroDipendenti;
		
		List<String> numeroPosti = new ArrayList<String>( Arrays.asList(
				numeroPostiDisponibili,
				numeroPostiOccupati
				));
		
		List<List<String>> array = new ArrayList<List<String>>( Arrays.asList(
				numeroPosti,
				nomeDipendenti
				));
		
		return new ResponseEntity<List<List<String>>>(array, HttpStatus.OK);
	}
	
	@GetMapping("/dipendentiMagazzinoPewex")
	public ResponseEntity<List<List<String>>> getDipendentiMagazzinoPewex() {
		List<Dipendente> dipendentiMagazzinoPewex = dipendenteRepository.findByMagazzinoId(supermercatoPewex().getMagazzino().getId());
		List<String> nomeDipendenti = dipendentiMagazzinoPewex.stream().map(d -> d.getNome()).collect(Collectors.toList());
		
		Long numeroDipendenti = dipendenteRepository.countNomeByMagazzinoId(supermercatoPewex().getOrtofrutta().getId());
		String numeroPostiDisponibili = "numero posti disponibili: " + ( 4 - numeroDipendenti ) ;
		String numeroPostiOccupati = "numero posti occupati: " +  numeroDipendenti;
		
		List<String> numeroPosti = new ArrayList<String>( Arrays.asList(
				numeroPostiDisponibili,
				numeroPostiOccupati
				));
		
		List<List<String>> array = new ArrayList<List<String>>( Arrays.asList(
				numeroPosti,
				nomeDipendenti
				));
		
		return new ResponseEntity<List<List<String>>>(array, HttpStatus.OK);
	}
	
	@GetMapping("/dipendentiCassaPewex")
	public ResponseEntity<List<List<String>>> findDipendentiCassaPewex() {
		List<Dipendente> dipendentiCassaPewex = dipendenteRepository.findByCassaId(supermercatoPewex().getCassa().getId());
		List<String> nomeDipendenti = dipendentiCassaPewex.stream().map(d -> d.getNome()).collect(Collectors.toList());
		
		Long numeroDipendenti = dipendenteRepository.countNomeByCassaId(supermercatoPewex().getOrtofrutta().getId());
		String numeroPostiDisponibili = "numero posti disponibili: " + ( 4 - numeroDipendenti ) ;
		String numeroPostiOccupati = "numero posti occupati: " +  numeroDipendenti;
		
		List<String> numeroPosti = new ArrayList<String>( Arrays.asList(
				numeroPostiDisponibili,
				numeroPostiOccupati
				));
		
		List<List<String>> array = new ArrayList<List<String>>( Arrays.asList(
				numeroPosti,
				nomeDipendenti
				));
		
		return new ResponseEntity<List<List<String>>>(array, HttpStatus.OK);
	}
	
	@PostMapping(value = "/assunzionePewex", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HttpStatus> assunzionePewex(@RequestBody DipendentePewex params) {
		
		Supermercato supermercatoPewex = supermercatoRepository.findByNome("Pewex");
		Dipendente dipendeteDaAssumere = new Dipendente(params.getNome());
		ResponseEntity<HttpStatus> response;
		
		switch (params.getReparto().toLowerCase()) {
		case "ortofrutta":
			supermercatoPewex.getOrtofrutta().addDipendente(dipendeteDaAssumere);
			response = new ResponseEntity<HttpStatus>(HttpStatus.OK);
			break;
		case "salumeria":
			supermercatoPewex.getSalumeria().addDipendente(dipendeteDaAssumere);
			response = new ResponseEntity<HttpStatus>(HttpStatus.OK);
			break;
		case "magazzino":
			supermercatoPewex.getMagazzino().addDipendente(dipendeteDaAssumere);
			response = new ResponseEntity<HttpStatus>(HttpStatus.OK);
			break;
		case "cassa":
			supermercatoPewex.getCassa().addDipendente(dipendeteDaAssumere);
			response = new ResponseEntity<HttpStatus>(HttpStatus.OK);
			break;
		default:
			throw new DipendenteException("impossibile assumere");
			
		}
		supermercatoRepository.save(supermercatoPewex);
		return response;
	
	}
	
	@GetMapping("/dipendente")
	public ResponseEntity<List<String>> repartiDipendente(@RequestParam String nome) {
		
		Dipendente dipendente = dipendenteRepository.findByNome(nome);
		
		System.out.println(dipendente);
		
		if ( dipendente == null ) {
			throw new DipendenteException("impossibile trovare il dipendente");
		}
		
		String nomeSupermercato = "supermercato " + dipendente.getSupermercato().getNome();
		
		List<String> dettagliDipendente = new ArrayList<String>(Arrays.asList(
				nomeSupermercato
				));
		
		if(dipendente.getCassa()!=null) {
			dettagliDipendente.add("cassa");
		}
		if(dipendente.getMagazzino()!=null) {
			dettagliDipendente.add("magazzino");
		}
		if(dipendente.getOrtofrutta()!=null) {
			dettagliDipendente.add("ortofrutta");
		}
		if(dipendente.getSalumeria()!=null) {
			dettagliDipendente.add("salumeria");
		}
		
		return new ResponseEntity<List<String>>(dettagliDipendente,HttpStatus.OK);
		
	}
	
	private Supermercato supermercatoPewex() {
		return supermercatoRepository.findByNome("Pewex");
	}
	
}
